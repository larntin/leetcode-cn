package utils;

public class ListNode {
	public int val;

	public ListNode next;

	ListNode() {
	}

	ListNode(int val) {
		this.val = val;
	}

	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}

	public static ListNode arr2List(int[] arr) {
		if (arr == null)
			return null;

		ListNode head = new ListNode(arr[0]);
		ListNode pre = head;
		for (int i = 1; i < arr.length; i++) {
			ListNode cur = new ListNode(arr[i]);
			pre.next = cur;
			pre = cur;
		}
		return head;
	}

	public static void logListNode(ListNode head) {
		ListNode it = head;
		while (it != null) {
			System.out.print("=>" + it.val + ", ");
			it = it.next;
		}
	}
}