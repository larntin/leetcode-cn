package leetcode.bit;

// 190. 颠倒二进制位

public class q180 {
	public static String dex2bin(int n) {
		String res = "";

		int r = n / 2;
		int q = n % 2;
		while (r != 0) {
			res = q + res;
			q = r % 2;
			r = r / 2;
		}

		// 最后如果是 1/2 = 0， 但是余的1要放到开头
		if (q == 1)
			res = q + res;

		return res;
	}

	public static String reverse(String input) {
		String r = "";
		for (int i = input.length() - 1; i >= 0; i--)
			r += input.charAt(i);
		return r;
	}

	public static void main(String[] argv) {
		System.out.println(dex2bin(65786));
		System.out.println(reverse(dex2bin(65786)));
		System.out.println(Integer.parseInt("01011111000000001", 2));
	}
}
