package leetcode;

// 977. 有序数组的平方
// 给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

public class q977 {

	public static int[] sortedSquares(int[] nums) {
		int p1 = 0;
		int p2 = nums.length - 1;
		int[] result = new int[nums.length];
		int pr = nums.length - 1;
		while (p1 <= p2) {
			if (nums[p1] * nums[p1] < nums[p2] * nums[p2]) {
				result[pr] = nums[p2] * nums[p2];
				p2--;
				pr--;
			} else {
				result[pr] = nums[p1] * nums[p1];
				p1++;
				pr--;
			}
		}
		return result;
	}

	public static void main(String[] argv) {
//		int[] nums = new int[] { -4, -1, 0, 3, 10 };
		int[] nums = new int[] { -7, -3, 2, 3, 11 };
		System.out.println(sortedSquares(nums));
	}
}
