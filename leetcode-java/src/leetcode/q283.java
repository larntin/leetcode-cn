package leetcode;

import java.util.ArrayList;
import java.util.Iterator;

// 283. 移动零 

// 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
// [0,1,0,3,12]
// [1,3,12,0,0]

public class q283 {

	public static void swap(int[] nums, int it1, int it2) {
		int tmp = nums[it1];
		nums[it1] = nums[it2];
		nums[it2] = tmp;
	}

	// 方法二：双指针法
	// 第一个指针指向已处理好的队尾，第二个指针指向未处理的队头
	public static int[] moveZeroes(int[] nums) {
		int p1 = 0;
		int p2 = 0;
		while (p2 < nums.length) {
			if (nums[p2] == 0)
				p2++;
			else {
				swap(nums, p1, p2);
				p1++;
				p2++;
			}
		}
		return nums;
	}

	// 解法1：
	// 使用一个ArrayList记录下来所有的0的位置
	// 然后从头开始遍历，使用当前位置的索引去记录0的位置的ArrayList搜索前面有几个0
	// 然后就将当前的字符往前移动几位
	// 最后数据后天填充多少个0
	// 效率比较低
	public static int countZero(ArrayList<Integer> arr, int index) {
		int result = 0;
		Iterator<Integer> it = arr.iterator();
		while (it.hasNext() && it.next() < index) {
			result++;
		}
		return result;
	}

	public static int[] moveZeroes_1(int[] nums) {
		int it = 0;
		ArrayList<Integer> zeroArr = new ArrayList<Integer>();
		while (it < nums.length) {
			if (nums[it] == 0)
				zeroArr.add(it);
			it++;
		}

		it = 0;
		while (it < nums.length) {
			if (nums[it] == 0)
				it++;
			else {
				int count = countZero(zeroArr, it);
				int tmp = nums[it - count];
				nums[it - count] = nums[it];
				nums[it] = tmp;
				it++;
			}
		}

		it = nums.length - zeroArr.size();
		while (it < nums.length) {
			nums[it++] = 0;
		}

		return nums;
	}

	public static void main(String[] argv) {
		int[] nums = new int[] { 0, 1, 0, 3, 12 };
		int[] result = moveZeroes(nums);
		System.out.println(result);
	}
}
