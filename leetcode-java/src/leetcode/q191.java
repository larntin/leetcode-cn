package leetcode;

//编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为汉明重量）。

//
//提示：
// 请注意，在某些语言（如 Java）中，没有无符号整数类型。在这种情况下，输入和输出都将被指定为有符号整数类型，并且不应影响您的实现，
// 因为无论整数是有符号的还是无符号的，其内部的二进制表示形式都是相同的。
// 在 Java 中，编译器使用二进制补码记法来表示有符号整数。因此，在上面的 示例 3 中，输入表示有符号整数 -3。

public class q191 {

	// 将 n（十进制） 转换为 二进制字符串的方法
	public static String tenTobin(int n) {
		int dev = n;
		String result = "";
		while (dev > 0) {
			int mod = dev % 2;
			result = mod + result;
			dev = dev / 2;
		}
		return result;
	}

	// 位运算1：
	// >> 算术右移，第一位按符号位填充，如果是负数，就会无线填充 1，导致无限循环
	// >>> 逻辑右移，直接右移，左边补0
	public static int rightMove(int n) {
		int result = 0;
		while (n != 0) {
			result += n & 1;
			n >>>= 1;
		}
		return result;
	}

	public static void test1() {
		System.out.println(rightMove(4));
	}

	public static void main(String[] argv) {
		test1();
	}
}
