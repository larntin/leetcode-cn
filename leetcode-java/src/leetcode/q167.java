package leetcode;

// 167. 两数之和 II - 输入有序数组
// 给定一个已按照 非递减顺序排列  的整数数组 numbers ，请你从数组中找出两个数满足相加之和等于目标数 target 。
// 函数应该以长度为 2 的整数数组的形式返回这两个数的下标值。numbers 的下标 从 1 开始计数 ，
// 所以答案数组应当满足 1 <= answer[0] < answer[1] <= numbers.length 。

public class q167 {

	public int[] twoSum(int[] numbers, int target) {
		int p1 = 0;
		int p2 = numbers.length - 1;

		while (p1 < p2) {
			if (numbers[p1] + numbers[p2] > target)
				p2--;
			else if (numbers[p1] + numbers[p2] < target)
				p1++;
			else {
				int[] result = new int[2];
				result[0] = p1 + 1;
				result[1] = p2 + 1;
				return result;
			}
		}
		return null;
	}

	public static void main(String[] argv) {
		int[] numbers = new int[] { 2, 7, 11, 15 };
		int target = 9;
		q167 t1 = new q167();
		int [] res = t1.twoSum(numbers, target);
		System.out.println(res);
	}
}
