package leetcode;

import utils.ListNode;

// 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
// 输入：l1 = [1,2,4], l2 = [1,3,4]
// 输出：[1,1,2,3,4,4]

public class q21 {

	public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		if (l1 == null && l2 != null)
			return l2;
		if (l2 == null && l1 != null)
			return l1;
		if (l1 == null && l2 == null)
			return null;

		ListNode p1 = l1;
		ListNode p2 = l2;

		ListNode head = l1;
		if (l1.val > l2.val)
			head = l2;

		while (p1 != null && p2 != null) {
			if (p1.val < p2.val && p1.next != null && p1.next.val <= p2.val) {
				// p1 后移
				p1 = p1.next;
			} else if (p2.val < p1.val) {
				// p2 插入当前 p1 之前
				ListNode t = p2;
				p2 = p2.next;
				t.next = p1;
				p1 = t;
//			} else if (p1.val <= p2.val && (p1.next != null && p2.val < p1.next.val)) {
			} else if (p1.val <= p2.val) {
				// p2 插入 p1 之后
				if (p1.next != null) {
					ListNode t = p2;
					p2 = p2.next;
					t.next = p1.next;
					p1.next = t;
					p1 = p1.next;
				} else {
					p1.next = p2;
					break;
				}
			}
		}

		return head;
	}

	public static void test1() {
		int[] arr1 = new int[] { 1, 2, 4 };
		ListNode it1 = ListNode.arr2List(arr1);
		ListNode.logListNode(it1);
	}

	public static void test2() {
		int[] arr1 = new int[] { 1, 2, 4 };
		int[] arr2 = new int[] { 1, 3, 4 };
		ListNode it1 = ListNode.arr2List(arr1);
		ListNode it2 = ListNode.arr2List(arr2);
		ListNode m = mergeTwoLists(it1, it2);
		ListNode.logListNode(m);
	}

	public static void test3() {
		int[] arr1 = new int[] { 2 };
		int[] arr2 = new int[] { 1 };
		ListNode it1 = ListNode.arr2List(arr1);
		ListNode it2 = ListNode.arr2List(arr2);
		ListNode m = mergeTwoLists(it1, it2);
		ListNode.logListNode(m);
	}

	public static void test4() {
		int[] arr1 = new int[] { 5 };
		int[] arr2 = new int[] { 1, 2, 4 };
		ListNode it1 = ListNode.arr2List(arr1);
		ListNode it2 = ListNode.arr2List(arr2);
		ListNode m = mergeTwoLists(it1, it2);
		ListNode.logListNode(m);
	}

	public static void main(String[] argv) {
		q21.test4();
	}
}
