package leetcode.search;

// 278. 第一个错误的版本
// 实现一个函数来查找第一个错误的版本。

public class q278 {
	static boolean isBadVersion(int version) {
		int bad = 4;
		return version >= bad;
	}

	public static int firstBadVersion(int n) {
		int left = 0;
		int right = n;
		while (left < right) {
			int mid = left + (right - left) / 2;
			if (isBadVersion(mid)) {
				// 坏的
				right = mid;
			} else {
				// 好的
				left = mid + 1;
			}
		}

		return left;
	}

	public static void main(String[] argv) {
		int res = firstBadVersion(5);
		System.out.println(res);
	}
}
