package leetcode.search;

// 704. 二分查找
// 给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target ，
// 写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。

//输入: nums = [-1,0,3,5,9,12], target = 9
//输出: 4
//解释: 9 出现在 nums 中并且下标为 4

public class q704 {

	// 递归的方式
	public static int binarySearch(int left, int right, int[] nums, int target) {
		int mid = left + (right - left) / 2;
		if (left > right)
			return -1;
		if (target > nums[mid]) {
			return binarySearch(mid + 1, right, nums, target);
		} else if (target < nums[mid]) {
			return binarySearch(left, mid - 1, nums, target);
		} else if (nums[mid] == target) {
			return mid;
		}

		return -1;
	}

	public static int search(int[] nums, int target) {
		int result = binarySearch(0, nums.length - 1, nums, target);
		return result;
	}

	public static void main(String[] argv) {
		int[] nums = new int[] { -1, 0, 3, 5, 9, 12 };
		int result = search(nums, 13);
		System.out.println(result);
	}
}
