package leetcode;

import java.util.Arrays;

// 189. 旋转数组
// 给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数

//输入: nums = [1,2,3,4,5,6,7], k = 3
//输出: [5,6,7,1,2,3,4]
//解释:
//向右旋转 1 步: [7,1,2,3,4,5,6]
//向右旋转 2 步: [6,7,1,2,3,4,5]
//向右旋转 3 步: [5,6,7,1,2,3,4]

public class q189 {

	// 按照官方的题解：
	// 第一次：反转整个数组
	// 第二次：反转 [0 - k]，和 [k, len - 1]
	public void reverseArray(int[] nums, int start, int end) {
		int tmp;
		while (start < end) {
			tmp = nums[start];
			nums[start] = nums[end];
			nums[end] = tmp;
			start++;
			end--;
		}
	}

	public void rotate(int[] nums, int k) {
		int tk = k % nums.length;
		this.reverseArray(nums, 0, nums.length - 1);
		this.reverseArray(nums, 0, tk - 1);
		this.reverseArray(nums, tk, nums.length - 1);
	}

	public static void main(String[] argv) {
		q189 t1 = new q189();
		int[] nums = new int[] { 1, 2, 3, 4, 5, 6, 7 };
		int k = 3;
		t1.rotate(nums, k);
		System.out.print(Arrays.toString(nums));
	}
}
