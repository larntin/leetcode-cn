package leetcode;

import java.util.ArrayList;
import java.util.List;

// 2 的幂
// 给你一个整数 n，请你判断该整数是否是 2 的幂次方。如果是，返回 true ；否则，返回 false 。
// 如果存在一个整数 x 使得 n == power(2, x) ，则认为 n 是 2 的幂次方。
//
// 进阶：你能够不使用循环/递归解决此问题吗？

public class q231 {

	public static int powerOfTwoR(int n) {
		if (n == 0)
			return 1;
		else if (n == 1)
			return 2;
		else
			return powerOfTwoR(n - 1) * 2;
	}

	public static int powerOfTwoF(int n) {
		int reuslt = 1;
		while (n > 0) {
			reuslt *= 2;
			n -= 1;
		}
		return reuslt;
	}

	public static boolean isPowerOfTwo(int n) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for (int i = 0; i <= 30; i++) {
			arr.add(powerOfTwoF(i));
		}
		return arr.indexOf(n) != -1;
	}

	public static void main(String[] argv) {
		System.out.println(isPowerOfTwo(17));
	}
}
