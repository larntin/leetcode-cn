/*
524. 通过删除字母匹配到字典里最长单词
给你一个字符串 s 和一个字符串数组 dictionary 作为字典，找出并返回字典中最长的字符串，该字符串可以通过删除 s 中的某些字符得到。

如果答案不止一个，返回长度最长且字典序最小的字符串。如果答案不存在，则返回空字符串。

 

示例 1：

输入：s = "abpcplea", dictionary = ["ale","apple","monkey","plea"]
输出："apple"
示例 2：

输入：s = "abpcplea", dictionary = ["a","b","c"]
输出："a"
 

提示：

1 <= s.length <= 1000
1 <= dictionary.length <= 1000
1 <= dictionary[i].length <= 1000
s 和 dictionary[i] 仅由小写英文字母组成
*/

$(function () {
  var findLongestWord = function (s, dictionary) {
    let resultStr = "";
    let resultLen = 0;
    for (let i = 0; i < dictionary.length; i += 1) {
      let curStr = dictionary[i];

      let sindex = 0;
      let dindex = 0;

      while (sindex < s.length && dindex < curStr.length) {
        if (curStr[dindex] == s[sindex]) {
          dindex += 1;
          sindex += 1;
        } else {
          sindex += 1;
        }
      }

      if (
        dindex == curStr.length && // 字符串全部匹配上
        (curStr.length > resultLen || // 当前字符串比之前的长直接记录
          (curStr.length == resultLen && curStr < resultStr)) // 当前和之前一样长，比较谁在英文字典序中小
      ) {
        resultStr = curStr;
        resultLen = resultStr.length;
      }
    }

    return resultStr;
  };

  // let s = "abpcplea";
  // let dictionary = ["ale", "apple", "monkey", "plea"];
  let s = "abce";
  let dictionary = ["abe", "abc"];
  console.log(findLongestWord(s, dictionary));
});
