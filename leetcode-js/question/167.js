/*
 */

$(function () {
  var twoSum = function (numbers, target) {
    let m = 0,
      n = 1;
    let found = false;
    for (; m < numbers.length - 1; m += 1) {
      n = m + 1;
      for (; n < numbers.length; n += 1) {
        if (numbers[n] + numbers[m] == target) {
          found = true;
          break;
        }
      }
      if (found) break;
    }

    return [m + 1, n + 1];
  };

  // let numbers = [5, 25, 75];
  let numbers = [2, 3, 4];
  let target = 6;
  console.log(twoSum(numbers, target));
});
