/*
给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

 

示例 1：

输入：nums = [-4,-1,0,3,10]
输出：[0,1,9,16,100]
解释：平方后，数组变为 [16,1,0,9,100]
排序后，数组变为 [0,1,9,16,100]
示例 2：

输入：nums = [-7,-3,2,3,11]
输出：[4,9,9,49,121]
 

提示：

1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums 已按 非递减顺序 排序
*/

$(function () {
  var sortedSquares = function (nums) {
    let p1 = 0;
    let p2 = nums.length - 1;
    let pos = nums.length - 1;
    let result = Array(nums.length);
    result.length = nums.length;
    while (p1 != p2) {
      let tp1 = nums[p1] * nums[p1];
      let tp2 = nums[p2] * nums[p2];
      if (tp1 < tp2) {
        result[pos] = tp2;
        p2 -= 1;
      } else {
        result[pos] = tp1;
        p1 += 1;
      }

      pos -= 1;
    }

    result[pos] = nums[p1] * nums[p1];

    return result;
  };

  // let nums = [-4, -1, 0, 3, 10];
  let nums = [-7, -3, 2, 3, 11];
  console.log(sortedSquares(nums));
});
