/*
 */

$(function () {
  var reverseString = function (s, start, end) {
    let swap;
    let left = start;
    let right = end;
    while (left < right) {
      swap = s[left];
      s[left] = s[right];
      s[right] = swap;
      left += 1;
      right -= 1;
    }
  };

  var reverseWords = function (s) {
    let cur = s.split("");
    let curSpace = cur.indexOf(" ");
    let wordStart = 0;
    let lastIndex = 0;
    while (curSpace != -1) {
      reverseString(cur, wordStart, curSpace - 1);
      wordStart = curSpace + 1;
      lastIndex = curSpace;
      curSpace = cur.indexOf(" ", wordStart);
    }
    if (lastIndex < cur.length)
      reverseString(cur, lastIndex + 1, cur.length - 1);

    return cur.join("");
  };

  let s = "Let's take LeetCode contest";
  console.log(reverseWords(s));
});
