/*
给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。

示例:

输入: [0,1,0,3,12]
输出: [1,3,12,0,0]
说明:

必须在原数组上操作，不能拷贝额外的数组。
尽量减少操作次数。
*/

$(function () {
  var moveZeroes = function (nums) {
    let p1 = 0,
      p2 = 0;

    if (nums.length < 2) return;

    debugger;

    // p2 指向第一个0
    while (nums[p2] != 0 && p2 < nums.length) p2 += 1;

    // p1 指向第一个0后的非0数
    p1 = p2;

    let swap;
    while (p1 < nums.length) {
      // p1 始终指向当前第一个非0的数
      if (nums[p1] == 0) {
        p1 += 1;
        continue;
      }

      // 交换 p1 和 p2 的值
      swap = nums[p1];
      nums[p1] = nums[p2];
      nums[p2] = swap;
      p2 += 1;
      p1 += 1;
    }
  };

  // let nums = [0, 1, 0, 3, 12];
  // let nums = [1, 0];
  let nums = [2, 1];
  moveZeroes(nums);
  console.log(nums);
});
