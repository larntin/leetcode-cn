$(function () {
  var findPeakElement = () => {
    let left = 0;
    let right = nums.length;

    while (left < right) {
      let mid = left + Math.floor((right - left) / 2);
      if (nums[mid] < nums[mid + 1]) {
        left = mid + 1;
      } else {
        right = mid;
      }
    }

    return right;
  };

  // var findPeakElement = function (nums) {
  //   for (let i = 1; i < nums.length - 1; i += 1) {
  //     if (nums[i - 1] < nums[i] && nums[i] > nums[i + 1]) {
  //       return i;
  //     }
  //   }
  // };

  // let nums = [1, 2, 3, 2];
  let nums = [1, 2, 1, 3, 5, 6, 4];
  console.log(findPeakElement(nums));
});
