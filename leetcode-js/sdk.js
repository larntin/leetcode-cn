// 单向链表数据结构
function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

// 列表转换成单向链表
function listToNode(arr) {
  let head = new ListNode(arr[0]);
  arr.reduce((pre, cur, curIndex, arr) => {
    if (curIndex == 0) return head;
    let curNode = new ListNode(cur);
    pre.next = curNode;
    return curNode;
  }, head);
  return head;
}

function logListNode(l, str = "") {
  while (l) {
    str = `${str}=>${l.val}`;
    l = l.next;
  }
  console.log(str);
}

/**
 * 多级双向链表中，除了指向下一个节点和前一个节点指针之外，它还有一个子链表指针，可能指向单独的双向链表。
 * 这些子列表也可能会有一个或多个自己的子项，依此类推，生成多级数据结构，如下面的示例所示。
 * // Definition for a Node.
 * function Node(val,prev,next,child) {
 *    this.val = val;
 *    this.prev = prev;
 *    this.next = next;
 *    this.child = child;
 * };
 */
