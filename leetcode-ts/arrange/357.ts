// 给你一个整数 n ，统计并返回各位数字都不同的数字 x 的个数，其中 0 <= x < 10n 。
// 0 <= n <= 8

// 解题思路：
// 当 n = 0 的时候，只有 { 0 } 一个数
// 当 n = 1 的时候，有 { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0} 10 个数
// 当 n = 2 的时候，左边第一位数只能从 1-9 中选择，而第二位数可以是除了第一个之外的9个数, 9 * A(1,9)
//    9 * 9 = 81 | 81 + 10 = 91
// 当 n = 3 的时候，左边第一位数只能从 1-9 中选择，而第二位数可以是除了第一个之外的9个数, 9 * A(1,9) * A(1,8)
//    9 * 9 * 8
// 当 n = 4 的时候，左边第一位数只能从 1-9 中选择，而第二位数可以是除了第一个之外的9个数, 9 * A(1,9) * A(1,8) * A(1,7)
function countNumbersWithUniqueDigits(n: number): number {
  if (n === 0) return 1;
  if (n === 1) return 10;

  let result = 10;
  let curNum = 0;
  for (let a = 2; a <= n; a++) {
    curNum = 9;
    for (let b = a; b > 1; b--) {
      curNum = curNum * (9 - (b - 2));
    }

    result += curNum;
  }

  return result;
}

export function main(): void {
  console.log(countNumbersWithUniqueDigits(4));
}
