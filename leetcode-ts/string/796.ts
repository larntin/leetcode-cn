// 796. 旋转字符串

// 给定两个字符串, s 和 goal。如果在若干次旋转操作之后，s 能变成 goal ，那么返回 true 。

// s 的 旋转操作 就是将 s 最左边的字符移动到最右边。

// 例如, 若 s = 'abcde'，在旋转一次之后结果就是'bcdea' 。

// 示例 1:
// 输入: s = "abcde", goal = "cdeab"
// 输出: true

// 方法1：模拟旋转之后的子串，然后递归寻找所有旋转之后的串中是否包含了目标串
function rotate(s: string, index: number): string {
  if (s.length === 1) return s;
  return s.substring(index) + s.substring(0, index);
}

function rotateString(s: string, goal: string): boolean {
  if (goal.length !== s.length) return false;

  let found: boolean = false;
  let index = 1;
  do {
    let r = rotate(s, index);
    found = r == goal;
    index++;
  } while (!found && index < s.length);
  return found;
}

export function main(): void {
  rotateString("abcde", "cdeab");
}

// 方法二：
// 思路
// 首先，如果 s 和 goal 的长度不一样，那么无论怎么旋转，s 都不能得到 goal，返回 false。
// 字符串 s + s 包含了所有 s 可以通过旋转操作得到的字符串，只需要检查 goal 是否为 s + s 的子字符串即可。
// 具体可以参考「28. 实现 strStr() 的官方题解」的实现代码，本题解中采用直接调用库函数的方法。

var rotateString2 = function (s, goal) {
  return s.length === goal.length && (s + s).indexOf(goal) !== -1;
};
