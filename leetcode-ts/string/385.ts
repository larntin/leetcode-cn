// 给定一个字符串 s 表示一个整数嵌套列表，实现一个解析它的语法分析器并返回解析的结果 NestedInteger 。
// 列表中的每个元素只可能是整数或整数嵌套列表

// This is the interface that allows for creating nested lists.
// You should not implement it, or speculate about its implementation
class NestedInteger {
  iVal: number = null;
  list: NestedInteger[] = [];

  constructor(value?: number) {
    if (value) this.iVal = value;
  }

  isInteger(): boolean {
    return !!this.iVal;
  }

  getInteger(): number | null {
    return this.iVal;
  }

  setInteger(value: number) {
    this.iVal = value;
  }

  add(elem: NestedInteger) {
    this.list.push(elem);
  }

  getList(): NestedInteger[] {
    return this.list;
  }
}

function parseOne(stack: any[], leftStr: string) {
  let index = 0;
  let abortSymbol = "[],";
  switch (leftStr[index]) {
    case "[":
      stack.push("[");
      parseOne(stack, leftStr.substring(1));
      break;

    case "]":
      {
        let oneLevel = [];
        let popitem;
        let curNi = new NestedInteger();
        do {
          popitem = stack.pop();
          if (popitem !== "[") oneLevel.push(popitem);
        } while (popitem !== "[");
        oneLevel
          .filter((o) => o !== ",")
          .reverse()
          .forEach((v) => curNi.add(v));
        stack.push(curNi);
        if (leftStr.length > 1) parseOne(stack, leftStr.substring(1));
      }
      break;

    case ",":
      stack.push(",");
      parseOne(stack, leftStr.substring(1));
      break;

    default:
      {
        let curString = "";
        while (abortSymbol.indexOf(leftStr.charAt(index)) === -1) {
          curString += leftStr.charAt(index);
          index += 1;
        }
        stack.push(new NestedInteger(parseInt(curString)));
        if (index < leftStr.length) parseOne(stack, leftStr.substring(index));
      }
      break;
  }
}

function deserialize(s: string): NestedInteger {
  let stack = [];
  let curStr = s;
  parseOne(stack, curStr);
  return stack[0];
}

export function main() {
  const test1 = "324";
  const test2 = "[123,[456,[789]]]";

  // console.log(deserialize(test1));
  console.log(deserialize(test2));
}
