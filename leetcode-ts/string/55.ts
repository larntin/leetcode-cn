// 55. 跳跃游戏
// 给定一个非负整数数组 nums ，你最初位于数组的 第一个下标 。
// 数组中的每个元素代表你在该位置可以跳跃的最大长度。
// 判断你是否能够到达最后一个下标。

//  示例 1：
// 输入：nums = [2,3,1,1,4]
// 输出：true
// 解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标。
// 示例 2：

// 输入：nums = [3,2,1,0,4]
// 输出：false
// 解释：无论怎样，总会到达下标为 3 的位置。但该下标的最大跳跃长度是 0 ， 所以永远不可能到达最后一个下标。

// 方法1：正向  贪心算法

// 贪心算法：
// 1，下标推进的过程，通过 max 记录可以到达的最右边界
// 2，收敛：当遇到收敛坑，例如 [3, 2, 1, 0, 4]，时需要识别出来
function canJump(nums: number[]): boolean {
  let rightMost = 0; // 可达到的最远下标记录
  for (let index = 0; index < nums.length; index++) {
    if (index <= rightMost) {
      // 识别收敛的关键条件，极值是rightMost，最后还需要判断一下 rightMost 当前值是否是坑中心0
      rightMost = Math.max(index + nums[index], rightMost);
      if (rightMost >= nums.length - 1) return true;
    }
  }
  return false;
}

// 方法2：最早开始位置  逆向算法
function canJump2(nums: number[]): boolean {
  let last: number = nums.length - 1;
  let i = nums.length - 2;  // 逆向递减下标
  for (; i >= 0; i--) {
    if (i + nums[i] >= last) last = i;
  }
  return last === 0;
}

export function main(): void {
  const nums1 = [2, 3, 1, 1, 4];
  const nums2 = [3, 2, 1, 0, 4];
  canJump(nums1);
  canJump2(nums2);
}
