// 变长数组可以在 O(1) 的时间内完成获取随机元素操作，但是由于无法在 O(1) 的时间内判断元素是否存在，因此不能在 O(1) 的时间内完成插入和删除操作。
// 哈希表可以在 O(1) 的时间内完成插入和删除操作，但是由于无法根据下标定位到特定元素，因此不能在 O(1) 的时间内完成获取随机元素操作。
// 为了满足插入、删除和获取随机元素操作的时间复杂度都是 O(1)，需要将变长数组和哈希表结合，变长数组中存储元素，哈希表中存储每个元素在变长数组中的下标。

class RandomizedSet {
  valMap;
  valArr;

  constructor() {
    this.valArr = [];
    this.valMap = {};
  }

  insert(val: number): boolean {
    if (!this.valMap[val] && this.valMap[val] != 0) {
      // 不存在
      this.valArr.push(val);
      this.valMap[val] = this.valArr.length - 1;
      return true;
    }
    return false;
  }

  remove(val: number): boolean {
    if (!this.valMap[val] && this.valMap[val] != 0)
      // 不存在
      return false;

    this.valArr.splice(this.valMap[val], 1);
    delete this.valMap[val];
    return true;
  }

  getRandom(): number {
    return this.valArr[Math.floor(Math.random() * this.valArr.length)];
  }
}

export function main() {
  let obj = new RandomizedSet();
  var param_1 = obj.insert(1);
  var param_2 = obj.remove(2);
  var param_3 = obj.insert(2);
  var param_4 = obj.getRandom();
  console.log(param_4);
  var param_5 = obj.remove(1);
  var param_6 = obj.remove(2);
  var param_7 = obj.getRandom();
  console.log(param_7);
}
